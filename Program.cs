﻿using System;
using System.Text;
using System.Threading.Tasks;

namespace pwsh5_bom_tests
{
    class Program
    {
        static async Task Main(string[] args)
        {
            byte[] nextLineBuffer = new byte[2048];

            await using var stdin = Console.OpenStandardInput();
            int bytes;
            while ((bytes = await stdin.ReadAsync(nextLineBuffer.AsMemory(0, nextLineBuffer.Length))) > 0)
            {
                var slicedBuffer = nextLineBuffer[..bytes];
                var hexedString = BitConverter.ToString(slicedBuffer);

                await Console.Out.WriteLineAsync($"Stdin hex: {hexedString}");

                Console.OutputEncoding = Encoding.ASCII;
                await Console.Out.WriteLineAsync($"Stdin parsed as ASCII: {Encoding.ASCII.GetString(slicedBuffer)}");

                Console.OutputEncoding = Encoding.UTF8;
                await Console.Out.WriteLineAsync($"Stdin parsed as UTF-8: {Encoding.UTF8.GetString(slicedBuffer)}");
                
                Console.OutputEncoding = Encoding.Unicode;
                await Console.Out.WriteLineAsync($"Stdin parsed as Unicode: {Encoding.Unicode.GetString(slicedBuffer)}");
            
            }
        }
    }
}
